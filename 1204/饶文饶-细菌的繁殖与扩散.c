#define _CRT_SECURE_NO_WARNINGS
#pragma warning(disable:4996)
#include<stdio.h>
#include<string.h>

int main() {
	int m, n;
	scanf("%d %d", &m, &n);
	int A[10][10] = { 0 };
	int B[10][10] = { 0 };
	B[4][4] = m;
	int i, j, k;

	for (k = 0; k < n; k++)
	{
		for (i = 0; i < 9; i++)
		{
			for (j = 0; j < 9; j++)
			{
				A[i][j] = B[i][j];
				B[i][j] = 2*B[i][j];
			}
		}

		for (i = 0; i < 9; i++)
		{
			for (j = 0; j < 9; j++)
			{
				if (A[i][j] != 0)
				{
					B[i - 1][j - 1] += A[i][j];
					B[i - 1][j] += A[i][j];
					B[i - 1][j + 1] += A[i][j];
					B[i][j - 1] += A[i][j];
					B[i][j + 1] += A[i][j];
					B[i + 1][j - 1] += A[i][j];
					B[i + 1][j] += A[i][j];
					B[i + 1][j + 1] += A[i][j];
				}
			}
		}
	}
	for (i = 0; i < 9;i++)
	{
		for (j = 0; j < 9; j++)
		{
			printf("%d ", B[i][j]);
		}
		printf("\n");
	}
	return 0;
}