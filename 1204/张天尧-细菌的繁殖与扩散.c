#include <stdio.h>
int main() {
    int m, n;
    int old[10][10] = { 0 }, new[10][10] = { 0 };
    scanf("%d %d", &m, &n); 
    old[5][5] = m; 
    for (int k = 1; k <= n; k++) 
    { 
        for (int i = 1; i <= 9; i++) 
        { 
            for (int j = 1; j <= 9; j++) 
            {
                if (old[i][j] != 0) { 
                    for (int x = -1; x <= 1; x++) 
                    { 
                        for (int y = -1; y <= 1; y++) 
                        { 
                            if (x == 0 && y == 0) 
                            { 
                                new[i + x][j + y] += 2 * old[i][j]; 
                            }
                            else 
                            {
                                new[i + x][j + y] += old[i][j];
                            }
                        }
                    }
                }
            }
        }
        for (int i = 1; i <= 9; i++)
        { 
            for (int j = 1; j <= 9; j++)
            { 
                old[i][j] = new[i][j];
                new[i][j] = 0; 
            }
        }
    }
    for (int i = 1; i <= 9; i++) 
    { 
        for (int j = 1; j <= 9; j++) 
        { 
            printf("%d ", old[i][j]); 
        }
        printf("\n"); 
    }
    return 0;
}