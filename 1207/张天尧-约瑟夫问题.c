#include <stdio.h>
int main()
{
    int n, m, out = 0;
    scanf("%d %d", &n,&m);
    int a[302] = { 0 };
    while (n != 0 && m != 0)
    {
        for (int i = 1; i <= n; i++)
        {
            a[i] = i;
        }
        while (n > 1)
        {
            out = (out + m - 1) % n;
            for (int j = out + 1; j < n; j++)
            {
                a[j] = a[j + 1];
            }
            n--;
            if (out == n)
            {
                out = 0;
            }
        }
        printf("%d\n", a[out + 1]);
        scanf("%d %d", &n, &m);
    }
    return 0;
}
