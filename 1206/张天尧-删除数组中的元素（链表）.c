#include <stdio.h>
#include <stdlib.h>

struct list {
	int data;
	int* next;
};

void del(int d, int n, struct list* head) {
	struct list* pr, * ne;
	pr = head;
	ne = head->next;
	int i;
	for (i = 0; i < n; i++) {
		if (ne->data == d) {
			pr->next = ne->next;
			ne->next = NULL;
			free(ne);
			ne = pr->next;
		}
		else {
			pr = ne;
			ne = ne->next;
		}
	}
}

int main() {
	int n, d, i;
	struct list* head = (struct list*)malloc(sizeof(struct list));
	struct list* pr, * ne;
	scanf("%d", &n);
	pr = head;
	for (i = 0; i < n; i++) {
		ne = (struct list*)malloc(sizeof(struct list));
		scanf("%d", &d);
		ne->data = d;
		ne->next = NULL;
		pr->next = ne;
		pr = ne;
	}
	/*ne=head->next;
	for (i=0;i<n;i++){
		printf("%d ",ne->data);
		ne=ne->next;
	}
	printf("\n");*/
	scanf("%d", &d);
	del(d, n, head);
	pr = head;
	ne = head->next;
	while (ne != NULL) {
		printf("%d ", ne->data);
		ne = ne->next;
	}
	return 0;
}