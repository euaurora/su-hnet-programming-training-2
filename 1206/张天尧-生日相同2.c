#include<stdio.h>
#include<stdlib.h>


struct stu
{
	char name[21];
	int mouth;
	int day;
};
struct stu student[200], ans[200];

/*int cmp(struct stu a, struct stu b) {
	if (strlen(a.name) < strlen(b.name))
	{
		return 1;
	}
	else if (strlen(a.name) > strlen(b.name))
	{
		return 0;
	}
	else
	{
		if (strcmp(a.name, b.name) < 0)
		{
			return 1;
		}
		if (strcmp(a.name, b.name) > 0)
		{
			return 0;
		}
	}
	return 0;
}*/

int compare(const void* e1, const void* e2)
{
	struct stu* p1 = (struct stu*)e1;
	struct stu* p2 = (struct stu*)e2;
	if (strlen(p1->name) > strlen(p2->name))
	{
		return 1;
	}
	else if (strlen(p1->name) == strlen(p2->name))
	{
		if (strcmp(p1->name, p2->name) > 0)
		{
			return 1;
		}
		if (strcmp(p1->name, p2->name) < 0)
		{
			return -1;
		}
	}
	else if (strlen(p1->name) <  strlen(p2->name))
	{
		return -1;
	}
}

int main()
{
	int n;
	scanf("%d", &n);
	for (int i = 1; i <= n; i++)
	{		
		scanf("%s %d%d", &student[i].name, &student[i].mouth, &student[i].day);
	}
	int flag = 0, count;
	for (int m = 1; m <= 12; m++) {
		for (int d = 1; d <= 31; d++) {
			count = 0;
			for (int i = 1; i <= n; i++)
			{
				if (student[i].mouth == m && student[i].day == d)
				{
					ans[count++] = student[i];
				}	
			}
			if (count > 1)
			{
				flag = 1;
				printf("%d %d ", m, d);
				qsort(ans, count, sizeof(struct stu), compare);
				for (int i = 0; i < count; i++) 
				{
					printf("%s ", ans[i].name);
				}
				printf("\n");
			}
		}
	}
	if (flag == 0)
	{
		printf("NONE");
	}
	return 0;
}