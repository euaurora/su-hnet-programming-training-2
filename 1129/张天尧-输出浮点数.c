#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>

void main()
{
	double f=0.0;

	scanf("%lf", &f);

	printf("%f\n", f);
	printf("%.5f\n", f);
	printf("%e\n", f);
	printf("%g\n", f);
	
	return 0;
}