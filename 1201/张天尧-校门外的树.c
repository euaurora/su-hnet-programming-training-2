#include<stdio.h>

int main()
{
    int i, j;
    int a[10000], l, m, s, e;
    int left = 0;

    scanf("%d%d", &l, &m);
    for (i = 0; i <= l; i++)
    {
        a[i] = 1;
    }
    for (i = 1; i <= m; i++)
    {
        scanf("%d %d", &s, &e);
        for (j = s; j <= e; j++)
        {
            a[j] = 0;
        }
    }
    for (i = 0; i <= l; i++)
    {
        if (a[i] == 1)
        {
            left++;
        }
    }
    printf("%d\n", left);
    return 0;
}