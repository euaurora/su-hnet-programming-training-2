#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
int main() {
	int L, M;
	scanf("%d%d", &L, &M);
	int tree[10001] = { 0 };
	for (int i = 1; i <= M; i++) {
		int begin, end;
		scanf("%d %d", &begin, &end);
		for (int j = begin; j <= end; j++) {
			tree[j] = 1;
		}
	}
	int sum = 0;
	for (int k = 0; k <= L; k++) {
		if (tree[k] == 0)
			sum++;
	}
	printf("%d", sum);
	return 0;
}
