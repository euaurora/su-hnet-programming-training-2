#define _CRT_SECURE_NO_WARNINGS
#pragma warning(disable:4996)
#include<stdio.h>
#include<string.h>

int main()
{
	char a[50];
	scanf("%s", &a);
	int l;
	l = strlen(a);

	for (int i = 0; i < l; i++)
	{
		if (a[i] <= 91)
		{
			a[i] = a[i] + 32;
		}
		else
		{
			a[i] = a[i] - 32;
		}
	}

	for (int j = 0; j < l; j++)
	{
		if (a[j] == 120 || a[j] == 121 || a[j] == 122 || a[j] == 90 || a[j] == 89 || a[j] == 88)
			a[j] = a[j] - 23;
		else
			a[j] = a[j] + 3;
	}

	for (int k = l - 1; k >= 0; k--)
	{
		printf("%c", a[k]);
	}
	return 0;
}