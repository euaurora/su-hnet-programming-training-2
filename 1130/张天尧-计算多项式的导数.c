#include <stdio.h>
int main()
{
    int n, c, i;
    scanf("%d", &n);
    if (n == 0) {
        printf("0");
        return 0;
    }
    for (i = n; i > 0; i--) {
        scanf("%d", &c);
        printf("%d ", c * i);
    }
    return 0;
}