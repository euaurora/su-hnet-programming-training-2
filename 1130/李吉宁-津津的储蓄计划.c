#include <stdio.h>

int main() {
    int budgets[12];
    int balance = 0, savings = 0;
    int month;
    for (month = 0; month < 12; month++) {
        scanf("%d", &budgets[month]);
    }
    for (month = 0; month < 12; month++) {
        balance =balance+ 300;  
        if (balance < budgets[month]) {
            printf("-%d\n", month + 1);
            return 0;
        }
        balance =balance- budgets[month];  
        savings = savings+(balance / 100) * 100;  
        balance %= 100; 
    }
    printf("%.0f\n", savings * 1.2 + balance);
    return 0;
}
